import defaultTheme from 'tailwindcss/defaultTheme'

export default {
  content: {
    relative: true,
    transform: (content) => content.replace(/taos:/g, ''),
    files: ['./**/*.{js,vue,blade.php,html}'],
  },
  plugins: [
    require('taos/plugin')
  ],
  theme: {
    extend: {
      
      fontFamily: {
        sans: ['"Cerebri Sans"', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  safelist: [
    '!duration-[0ms]',
    '!delay-[0ms]',
    'html.js :where([class*="taos:"]:not(.taos-init))'
  ]
}
