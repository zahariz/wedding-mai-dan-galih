const express = require('express');
const sqlite3 = require('sqlite3').verbose();

const app = express();
const port = 3000;

const path = require('path');

app.use('/', express.static(path.join(__dirname, '')));

// Buat koneksi ke database SQLite
let db = new sqlite3.Database('my_database.db', sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  } else {
    console.log('Connected to the database.');
  }
});

// Buat tabel jika belum ada
db.serialize(() => {
  db.run(`CREATE TABLE IF NOT EXISTS guestbook (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    message TEXT,
    confirm TEXT
  )`);
  db.run(`
    CREATE TABLE IF NOT EXISTS link (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT
  )`);
});

// Middleware untuk mem-parse body dari request
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Route untuk halaman utama
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
app.get('/buat-tamu', (req, res) => {
  res.sendFile(__dirname + '/undangan.html');
});

// Atur route untuk menampilkan data tamu
app.get('/guests', (req, res) => {
    // Query untuk mengambil data tamu dari database
    const query = 'SELECT * FROM guestbook';

    // Lakukan query ke database
    db.all(query, (err, rows) => {
        if (err) {
            // Tangani kesalahan jika ada
            console.error(err.message);
            res.status(500).send('Terjadi kesalahan dalam mengambil data tamu.');
        } else {
            // Kirim data tamu sebagai respons
            res.json(rows);
        }
    });
});

app.get('/linktamus', (req, res) => {
    // Query untuk mengambil data tamu dari database
    const query = 'SELECT * FROM link';

    // Lakukan query ke database
    db.all(query, (err, rows) => {
        if (err) {
            // Tangani kesalahan jika ada
            console.error(err.message);
            res.status(500).send('Terjadi kesalahan dalam mengambil data tamu.');
        } else {
            // Kirim data tamu sebagai respons
            res.json(rows);
        }
    });
});

// Route untuk menyimpan data tamu
app.post('/guestbook', (req, res) => {
  const { name, message, confirm } = req.body;
  saveGuestbookEntry(name, message, confirm);
  res.send('Data tamu berhasil disimpan.');
});

app.post('/link', (req, res) => {
    const { name } = req.body;
    saveLink(name);
    res.send('Data tamu berhasil disimpan.');
  });


// Simpan data tamu ke database
function saveGuestbookEntry(name, message, confirm) {
  db.run('INSERT INTO guestbook (name, message, confirm) VALUES (?, ?, ?)', [name, message, confirm], (err) => {
    if (err) {
      console.error(err.message);
    } else {
      console.log('New entry added to guestbook.');
    }
  });
}

function saveLink(name) {
    db.run('INSERT INTO link (name) VALUES (?)', [name], (err) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log('New entry added to guestbook.');
      }
    });
  }

// Jalankan server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
